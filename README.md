# contactMe

If you've been invited into this project, it means that I appreciate your contacting me on gitlab, and would like you to send me an email. Gitlab lacks features which would let you introduce yourself, let alone features so that we could chat.

You can find my email address at [qrz.com/db/w6el](https://qrz.com/db/w6el) or within [the source code](https://gitlab.com/eliggett/wfview/-/blob/master/aboutbox.cpp) to wfview. Send me an email, I'd like to hear from you!


Gitlab, please add a feature so that requests can come with a comment and support replies!

![Humor about lack of communication features on gitlab](https://gitlab.com/eliggett/contactme/-/raw/main/change-my-mind.jpeg "Humor")
